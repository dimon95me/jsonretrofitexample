package examplesoureit.jsonretrofitexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import examplesoureit.jsonretrofitexample.model.Feed;
import examplesoureit.jsonretrofitexample.model.children.Children;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private static final String BASE_URL = "https://www.reddit.com/";

    @BindView(R.id.btn_get_data)
    Button action;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);




    }

    @OnClick(R.id.btn_get_data)
    public void onActionClick(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RetrofitAPI retrofitAPI = retrofit.create(RetrofitAPI.class);
        Call<Feed> call = retrofitAPI.getData();

        call.enqueue(new Callback<Feed>() {
            @Override
            public void onResponse(Call<Feed> call, Response<Feed> response) {
                Log.d(TAG, "onResponse: Server Respnce: "+response.toString());
                Log.d(TAG, "onResponse: received information: "+ response.body().toString());

                ArrayList<Children> childrenArrayList  = response.body().getData().getChildren();
                for (int i = 0; i < childrenArrayList.size(); i++) {
                    Log.d(TAG, "onResponse: \n" +
                            "kind: " + childrenArrayList.get(i).getKind() + "\n" +
                            "contest_mode: " + childrenArrayList.get(i).getData().getContest_mode() + "\n" +
                            "subreddit: " + childrenArrayList.get(i).getData().getSubreddit()  + "\n" +
                            "author: " + childrenArrayList.get(i).getData().getAuthor()  + "\n" +
                            "-------------------------------------------------------------------------\n\n");
                }
            }

            @Override
            public void onFailure(Call<Feed> call, Throwable t) {
                Log.e(TAG, "onFailure: Something went wrong: "+t.getMessage());
                Toast.makeText(MainActivity.this, "Something went wrong: "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
