package examplesoureit.jsonretrofitexample.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import examplesoureit.jsonretrofitexample.model.children.Children;

public class Data {

    @SerializedName("modhash")
    @Expose
    private String modhash;

    @SerializedName("kind")
    @Expose
    private ArrayList<Children> children;

    @Override
    public String toString() {
        return "Data{" +
                "modhash='" + modhash + '\'' +
                ", children=" + children +
                '}';
    }

    public String getModhash() {
        return modhash;
    }

    public void setModhash(String modhash) {
        this.modhash = modhash;
    }

    public ArrayList<Children> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<Children> children) {
        this.children = children;
    }
}
